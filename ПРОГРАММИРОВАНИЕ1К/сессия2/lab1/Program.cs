﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Решение уравнения a*x=b:");
            Console.Write("Введите a: ");
            Single a = Convert.ToSingle(Console.ReadLine());
            Console.Write("Введите b: ");
            Single b = Convert.ToSingle(Console.ReadLine());

            if (a == 0)
                if (b != 0)
                    Console.WriteLine("x не сущестует");
                else
                    Console.WriteLine("x равен любому числу");
            else
                Console.WriteLine("x = " + b/a);

            Console.WriteLine("Нажмите любую клавишу, чтобы выйти");
            Console.ReadKey();
        }
    }
}
