A = [[0,-3,5],
    [0, 0, 6],
    [0,4,5]]

n = len(A)  
det = 1
k = 0
while k<n:
    if A[k][k] == 0:
        i = k
        while i<n:
            if A[i][k] != 0:
                for j in range(k,n):
                    A[k][j],A[i][j]=A[i][j],A[k][j]
            i+=1
    if A[k][k]!=0:
        det*=A[k][k]
    for i in range(k+1,n):
        if A[i][k] != 0:
            for j in range(k,n):
                A[i][j] -= A[k][j]*A[i][k]/A[k][k]
    k+=1
print(det)