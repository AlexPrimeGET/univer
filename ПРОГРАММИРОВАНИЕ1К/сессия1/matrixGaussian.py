A = [[1,2,5,5],
    [0, 0, 5,-4],
    [1,0,5,5]]

n = len(A)
res = [0]*n
k = 0
while k<n:
    if A[k][k] == 0:
        i = k
        while i<n:
            if A[i][k] != 0:
                for j in range(k,n+1):
                    A[k][j],A[i][j]=A[i][j],A[k][j]
            i+=1
    if A[k][k]==0:
        print('no solution')
        exit(0)
    for i in range(k+1,n):
        if A[i][k] != 0:
            for j in range(k,n+1):
                A[i][j] -= A[k][j]*A[i][k]/A[k][k]
    k+=1
k = n-1
while k > -1:
    for i in range(k+1,n):    
        A[k][n] -= A[k][i]*res[i]
        print(k+1,n)
    res[k] = A[k][n]/A[k][k]
    k-=1
print(res)