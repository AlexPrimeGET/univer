﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace facade
{
    class Delivery
    {
        Warehouse warehouse;
        Fabric fabric;

        public Delivery()
        {
            this.fabric = new Fabric();
            this.warehouse = new Warehouse();
            Random rnd;
            for (int i = 0; i < 10; i++)
            {
                rnd = new Random(i);
                warehouse.Add(
                    fabric.createProduct(rnd.Next(1, 10), (new string[] { "Phone", "Computer", "Laptop" })[rnd.Next(0, 3)])
                    );
            }
            Console.WriteLine("Write commands in form: '{command} {argument}' \n" +
                              "To order delivery use 'order {product id}' \n" +
                              "To get list of products use 'list' \n" +
                              "To exit use 'exit'");
        }
        public void command(string line)
        {
            string[] split = line.Split(' ');
            string type = split[0];
            if (type == "list")
            {
                Console.WriteLine("id type price");
                Console.WriteLine(String.Join("\n", getProductList()));
            }
            else if (type == "order")
            {
                int id = Convert.ToInt32(split[1]);

                if (warehouse.products.Count >= id)
                {
                    Console.WriteLine($"You ordered {deliver(id).type}"); 
                }
                else Console.WriteLine("Wrong id");
            }
            else if (type == "exit")
            {
                Environment.Exit(200);
            }
        }
        private List<string> getProductList()
        {
            List<string> list = new List<string> { };
            int index = 0;
            foreach (Product prod in warehouse.products)
            {
                list.Add($"{index++} {prod.type} {prod.price}");
            }
            return list;
        }
        private Product deliver(int index)
        {
            warehouse.Add(
                fabric.createProduct(warehouse.products[index].price, warehouse.products[index].type)
                );
            return warehouse.TakeFrom(index);
        }
    }
}
