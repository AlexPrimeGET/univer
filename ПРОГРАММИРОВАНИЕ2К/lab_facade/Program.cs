﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace facade
{
    class Program
    {
        static void Main(string[] args)
        {
            Delivery amazon = new Delivery();

            while (true)
            {
                amazon.command(Console.ReadLine());
            }
        }
    }
}
