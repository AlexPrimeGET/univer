﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace facade
{
    struct Product
    {
        public int price;
        public string type;

        public Product(int price, string type)
        {
            this.price = price;
            this.type = type;
        }
    }
    class Fabric
    {
        public Product createProduct(int price, string type)
        {
            return new Product(price, type);
        }
    }
}
