﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace command
{
    class Warehouse
    {
        public List<Product> products;
        public Warehouse()
        {
            products = new List<Product>();
        }
        public void Add(Product product)
        {
            products.Add(product);
        }
        public Product TakeFrom(int index)
        {
            var product = products[index];

            products.RemoveAt(index);

            return product;
        }
    }
}
