﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace command
{
    class Program
    {
        static void Main(string[] args)
        {
            Delivery amazon = new Delivery("12345");
            Delivery.ICommand cmd;

            Console.WriteLine("Write commands in form: '{command} {argument}' \n" +
                              "To order delivery use 'order {product id}' \n" +
                              "To get list of products use 'list' \n" +
                              "To authorize use 'auth {password}' \n" +
                              "To exit use 'exit'");
            while (true)
            {
                string[] split = Console.ReadLine().Split(' ');
                switch (split[0])
                {
                    case "list":
                        cmd = new Delivery.CommandGetList(amazon);
                        if (amazon.auth)
                        {
                            Console.WriteLine(cmd.execute());
                        }
                        else Console.WriteLine(cmd.reject());
                        break;
                    case "order":
                        cmd = new Delivery.CommandOrder(amazon, Convert.ToInt32(split[1]));
                        if (amazon.auth)
                        {
                            Console.WriteLine(cmd.execute());
                        }
                        else
                        {
                            cmd.reject();
                            Console.WriteLine("Unauthorized");
                        }
                        break;
                    case "auth":
                        cmd = new Delivery.CommandAuth(amazon, split[1]);
                        if (amazon.auth) Console.WriteLine(cmd.reject());
                        else Console.WriteLine(cmd.execute());
                        break;
                    case "exit":
                        cmd = new Delivery.CommandClose();
                        Console.WriteLine("Are you sure? y/n");
                        if (Console.ReadLine() == "y") cmd.execute();
                        else Console.WriteLine(cmd.reject());
                        break;
                    default: 
                        Console.WriteLine("No Suitable Command");
                        break;
                }
            }
        }
    }
}
