﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace command
{
    class Delivery
    {
        Warehouse warehouse;
        Fabric fabric;
        public bool auth;
        private string password;

        public Delivery(string password)
        {
            this.password = password;
            this.fabric = new Fabric();
            this.warehouse = new Warehouse();
            Random rnd;
            for (int i = 0; i < 10; i++)
            {
                rnd = new Random(i);
                warehouse.Add(
                    fabric.createProduct(rnd.Next(1, 10), (new string[] { "Phone", "Computer", "Laptop" })[rnd.Next(0, 3)])
                    );
            }
            this.auth = false;
        }
        public interface ICommand
        {
            string execute();
            string reject();
        }
        public class CommandClose : ICommand
        {
            public string execute()
            {
                Environment.Exit(200);
                return "200";
            }
            public string reject()
            {
                return "OK";
            }
        }
        public class CommandGetList : ICommand
        {
            private Delivery parent;
            public CommandGetList(Delivery parent)
            {
                this.parent = parent;
            }
            public string execute()
            {
                return String.Join("\n", parent.getProductList());
            }
            public string reject()
            {
                return "Unauthorized";
            }
        }
        public class CommandOrder : ICommand
        {
            private Delivery parent;
            int id;
            public CommandOrder(Delivery parent, int id)
            {
                this.parent = parent;
                this.id = id;
            }
            public string execute()
            {
                if (id >= 0 && parent.warehouse.products.Count > id)
                    return parent.deliver(id).type;
                else return reject();
            }
            public string reject()
            {
                return "Bad Request";
            }
        }
        public class CommandAuth : ICommand
        {
            private Delivery parent;
            string password;
            public CommandAuth(Delivery parent, string password)
            {
                this.parent = parent;
                this.password = password;
            }
            public string execute()
            {
                bool response = parent.checkPassword(password);
                if (response)
                {
                    parent.auth = response;
                    return "Successfull";
                }
                else return "Wrong password";
            }
            public string reject()
            {
                return "Forbidden";
            }
        }
        private List<string> getProductList()
        {
            List<string> list = new List<string> { };
            int index = 0;
            foreach (Product prod in warehouse.products)
            {
                list.Add($"{index++} {prod.type} {prod.price}");
            }
            return list;
        }
        private Product deliver(int index)
        {
            warehouse.Add(
                fabric.createProduct(warehouse.products[index].price, warehouse.products[index].type)
                );
            return warehouse.TakeFrom(index);
        }
        public bool checkPassword(string password)
        {
            return (this.password == password);
        }
    }
}
