﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Computer
    {
        public string firm;
        public string color = "White";
        public double outputPower;
        public virtual void setData(string firm, string color, double outputPower)
        {
            this.firm = firm;
            this.color = color;
            this.outputPower = outputPower;
        }
    }
    class AdvancedComputer : Computer
    {
        public new string color = "Black";
        private bool soundIsolated;
        public override void setData(string firm, string color, double outputPower)
        {
            base.setData(firm, color, outputPower);
            this.color = color;
            this.soundIsolated = false;
        }
        public void setIsolation()
        {
            this.soundIsolated = true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Computer standComp = new Computer();
            Console.WriteLine(standComp.color);
            standComp.setData("test1", "color1", 10);
            Console.WriteLine(standComp.color);

            AdvancedComputer advComp = new AdvancedComputer();
            Console.WriteLine(advComp.color);
            advComp.setData("test2", "color2", 20);
            Console.WriteLine(advComp.color);
            Console.ReadLine();
        }
    }
}
