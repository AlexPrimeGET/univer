﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class Computer
    {
        public string firm;
        public string color;
        public double outputPower;

        public Computer()
        {
            this.firm = "[void]";
            this.color = "[void]";
            this.outputPower = 0;
        }
        public Computer(string firm, string color, double outputPower)
        {
            this.firm = firm;
            this.color = color;
            this.outputPower = outputPower;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Computer compStandart = new Computer();
            Computer compOverloaded = new Computer("test", "white", 20);
            Console.WriteLine($"{compStandart.firm}, {compStandart.color}, {compStandart.outputPower}\n" +
                $"{compOverloaded.firm}, {compOverloaded.color}, {compOverloaded.outputPower}");
            Console.ReadLine();
        }
    }
}
