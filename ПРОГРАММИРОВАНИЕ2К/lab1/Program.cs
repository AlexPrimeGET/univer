﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class Computer
    {
        public static string firm = "test";
        public static string color = "test";
        public static double outputPower = 0;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"{Computer.firm}, {Computer.color}, {Computer.outputPower}");
            Console.ReadLine();
        }
    }
}
