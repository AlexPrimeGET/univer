﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace lab5
{
    [Serializable]
    class Computer
    {
        public string firm;
        public string color;
        public double outputPower;
        public Computer() { }
        public Computer(string firm, string color, double outputPower)
        {
            this.firm = firm;
            this.color = color;
            this.outputPower = outputPower;
        }
        public void set(string firm, string color, double outputPower)
        {
            this.firm = firm;
            this.color = color;
            this.outputPower = outputPower;
        }
    }
    [Serializable]
    class Computers
    {

        public Computer[] array;
        public int Length;
        public Computers(int length)
        {
            this.Length = length;
            this.array = new Computer[length];
            for (int i = 0; i < length; i++)
            {
                this.array[i] = new Computer();
            }
        }
        public Computer this[int index]
        {
            get
            {
                return this.array[index];
            }
            set
            {
                this.array[index] = value;
            }
        }

    }
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Computer test = new Computer("test", "test", 50);
            Computers tests = new Computers(2);
            tests[0].set("test", "test", 50);
            tests[1].set("test1", "test1", 500);

            FileStream fileStream = new FileStream("Computers.bin",FileMode.Create,FileAccess.Write);

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, test);
            binaryFormatter.Serialize(fileStream, tests);
            fileStream.Close();

            fileStream = new FileStream("Computers.bin", FileMode.Open, FileAccess.Read);

            Computer test1 = (Computer)binaryFormatter.Deserialize(fileStream);
            Computers tests1 = (Computers)binaryFormatter.Deserialize(fileStream);

            Console.WriteLine(test1.firm);
            Console.WriteLine(tests1[1].firm);
            Console.ReadLine();
        }
    }
}
