﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace observer
{
    class Blog : ISubject
    {
        private List<IObserver> subscribers;
        public string lastPostValue;
        public string lastPostState;

        public Blog()
        {
            this.subscribers = new List<IObserver> { };
        }

        public void subscribe(IObserver subscriber)
        {
            subscribers.Add(subscriber);
        }
        public void unsubscribe(IObserver subscriber)
        {
            subscribers.Remove(subscriber);
        }
        public void notify()
        {
            foreach(IObserver sub in subscribers)
            {
                sub.update(this);
            }
        }
        public void post(string postValue, string postState)
        {
            this.lastPostValue = postValue;
            this.lastPostState = postState;
            this.notify();
        }
    }
    
    class Subscriber : IObserver
    {
        public void update(ISubject subject)
        {
            if ((subject as Blog).lastPostState == "normal")
            {
                Console.WriteLine((subject as Blog).lastPostValue);
            }
        }
    }
    class SubscriberPlus: IObserver
    {
        public void update(ISubject subject)
        {
            Console.WriteLine((subject as Blog).lastPostValue);
        }
    }
}
