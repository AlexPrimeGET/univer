﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace observer
{
    class Program
    {
        static void Main(string[] args)
        {
            Blog blog = new Blog();
            Subscriber sub = new Subscriber();
            SubscriberPlus subPlus = new SubscriberPlus();

            blog.subscribe(sub);
            blog.subscribe(subPlus);

            blog.post("Всем привет", "normal");
            blog.post("Спасибо вам за поддержку", "plus");

            blog.unsubscribe(sub);

            blog.post("И снова привет, друзь... друг", "normal");

            Console.ReadLine();
        }
    }
}
