﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace observer
{
    interface IObserver
    {
        void update(ISubject subject);
    }
    interface ISubject
    {
        void subscribe(IObserver observer);
        void unsubscribe(IObserver observer);
        void notify();
    }
}
