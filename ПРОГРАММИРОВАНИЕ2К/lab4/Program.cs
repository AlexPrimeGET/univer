﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Computer
    {
        public string firm;
        public string color;
        public double outputPower;
        public Computer() 
        {
            this.firm = "test";
            this.color = "test";
        }
    }
    class Computers
    {
        
        public Computer[] array;
        public int Length;
        public Computers (int length)
        {
            this.Length = length;
            this.array = new Computer[length];
            for (int i = 0; i<length; i++)
            {
                this.array[i] = new Computer();
                this.array[i].firm += i+1;
            }
        }
        public Computer this[int index]
        {
            get
            {
                return this.array[index];
            }
            set
            {
                this.array[index] = value;
            }
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите требуемое количество элементов: ");
            Computers tests = new Computers( Convert.ToInt32(Console.ReadLine()) );
            for (int i = 0; i < tests.Length; i++) {
                Console.WriteLine($"{tests[i].firm} {tests[i].color} {tests[i].outputPower}");
            }
            Console.ReadLine();
        }
    }
}
