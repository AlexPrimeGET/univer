﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7
{
    class Computer
    {
        public string firm { get; set; }
        public string color { get; set; }
        public double outputPower { get; set; }
        public Computer() { }
        public Computer(string firm, string color, double outputPower)
        {
            this.firm = firm;
            this.color = color;
            this.outputPower = outputPower;
        }
        public static bool operator <(Computer a, Computer b) { if (a.outputPower < b.outputPower) return true; else return false; }
        public static bool operator >(Computer a, Computer b) { if (a.outputPower > b.outputPower) return true; else return false; }
        public void setData(string firm, string color, double outputPower)
        {
            this.firm = firm;
            this.color = color;
            this.outputPower = outputPower;
        }
        public void copyDataTo(Computer computer)
        {
            computer.firm = this.firm;
            computer.color = this.color;
            computer.outputPower = this.outputPower;
        }
    }
    class Computers
    {
        public BindingList<Computer> array;
        public int Length;
        public Computers()
        {
            this.Length = 0;
            this.array = new BindingList<Computer>();
        }
        public Computers(int length)
        {
            this.Length = length;
            this.array = new BindingList<Computer>();
            for (int i = 0; i < length; i++)
            {
                this.array.Add(new Computer());
            }
        }
        public Computers(Computer[] array)
        {
            this.Length = array.Length;
            this.array = new BindingList<Computer>();
            for (int i = 0; i < this.Length; i++)
            {
                this.array.Add(array[i]);
            }
        }
        public Computer this[int index]
        {
            get
            {
                return this.array[index];
            }
            set
            {
                this.array[index] = value;
            }
        }
        public void Add(Computer computer)
        {
            this.array.Add(computer);
            this.Length++;
        }
        public void Remove(Computer computer)
        {
            this.array.Remove(computer);
            this.Length--;
        }
        public void selectionSort()
        {
            if (this.Length < 2) return;
            Computer min = new Computer();
            this.array[0].copyDataTo(min);
            int minIndex;

            for (int i = 0; i < this.Length; i++)
            {
                minIndex = i;
                for (int j = i; j < this.Length; j++)
                {
                    if (this.array[j] < this.array[minIndex])
                    {
                        minIndex = j;
                    }
                }
                this.array[minIndex].copyDataTo(min);
                this.array[i].copyDataTo(this.array[minIndex]);
                min.copyDataTo(this.array[i]);
            }
        }
        public void bubbleSort()
        {
            if (this.Length < 2) return;
            Computer replacer = new Computer();
            for (int i = this.Length - 2; i >= 0; i--)
            {
                for (int j = 0; j <= i; j++)
                {
                    if (this.array[j] > this.array[j + 1])
                    {
                        this.array[j].copyDataTo(replacer);
                        this.array[j + 1].copyDataTo(this.array[j]);
                        replacer.copyDataTo(this.array[j + 1]);
                    }
                }
            }
        }
        public void inclusionSort()
        {
            if (this.Length < 2) return;
            Computer current = new Computer();
            int index;
            for (int i = 1; i < this.Length; i++)
            {
                this.array[i].copyDataTo(current);
                index = i;
                while ((index > 0) && (array[index - 1] > current))
                {
                    array[index - 1].copyDataTo(array[index]);
                    index--;
                }
                current.copyDataTo(array[index]);
            }
        }
        public void shakerSort()
        {
            if (this.Length < 2) return;
            Computer replacer = new Computer();
            for (int i = 0; i < (this.Length / 2) + (this.Length % 2); i++)
            {
                for (int j = i; j < (this.Length - 1); j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        array[j].copyDataTo(replacer);
                        array[j + 1].copyDataTo(array[j]);
                        replacer.copyDataTo(array[j + 1]);
                    }
                }
                for (int j = this.Length - 1 - i; j > i; j--)
                {
                    if (array[j] < array[j - 1])
                    {
                        array[j].copyDataTo(replacer);
                        array[j - 1].copyDataTo(array[j]);
                        replacer.copyDataTo(array[j - 1]);
                    }
                }
            }
        }
        public void shellSort()
        {
            if (this.Length < 2) return;
            Computer replacer = new Computer();
            for (int s = this.Length / 2; s > 0; s /= 2)
                for (int i = s; i < this.Length; i++)
                    for (int j = i - s; j >= 0 && array[j] > array[j + s]; j -= s)
                    {
                        array[j].copyDataTo(replacer);
                        array[j + s].copyDataTo(array[j]);
                        replacer.copyDataTo(array[j + s]);
                    }
        }
    }
}
