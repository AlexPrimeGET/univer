﻿namespace lab7
{
    partial class Form
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.сортировкаСпискаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.методПрямогоВыбораToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.методПузырькаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.методПрямогоВключенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шейкерныйМетодToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.методШеллаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.рандомныйСписокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сортировкаСпискаToolStripMenuItem,
            this.рандомныйСписокToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(795, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // сортировкаСпискаToolStripMenuItem
            // 
            this.сортировкаСпискаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.методПрямогоВыбораToolStripMenuItem,
            this.методПузырькаToolStripMenuItem,
            this.методПрямогоВключенияToolStripMenuItem,
            this.шейкерныйМетодToolStripMenuItem,
            this.методШеллаToolStripMenuItem});
            this.сортировкаСпискаToolStripMenuItem.Name = "сортировкаСпискаToolStripMenuItem";
            this.сортировкаСпискаToolStripMenuItem.Size = new System.Drawing.Size(124, 20);
            this.сортировкаСпискаToolStripMenuItem.Text = "Выбор сортировки";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 497);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 17, 0);
            this.statusStrip1.Size = new System.Drawing.Size(795, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 31);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(795, 474);
            this.dataGridView1.TabIndex = 2;
            // 
            // методПрямогоВыбораToolStripMenuItem
            // 
            this.методПрямогоВыбораToolStripMenuItem.Name = "методПрямогоВыбораToolStripMenuItem";
            this.методПрямогоВыбораToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.методПрямогоВыбораToolStripMenuItem.Text = "Метод прямого выбора";
            this.методПрямогоВыбораToolStripMenuItem.Click += new System.EventHandler(this.методПрямогоВыбораToolStripMenuItem_Click);
            // 
            // методПузырькаToolStripMenuItem
            // 
            this.методПузырькаToolStripMenuItem.Name = "методПузырькаToolStripMenuItem";
            this.методПузырькаToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.методПузырькаToolStripMenuItem.Text = "Метод пузырька";
            this.методПузырькаToolStripMenuItem.Click += new System.EventHandler(this.методПузырькаToolStripMenuItem_Click);
            // 
            // методПрямогоВключенияToolStripMenuItem
            // 
            this.методПрямогоВключенияToolStripMenuItem.Name = "методПрямогоВключенияToolStripMenuItem";
            this.методПрямогоВключенияToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.методПрямогоВключенияToolStripMenuItem.Text = "Метод прямого включения";
            this.методПрямогоВключенияToolStripMenuItem.Click += new System.EventHandler(this.методПрямогоВключенияToolStripMenuItem_Click);
            // 
            // шейкерныйМетодToolStripMenuItem
            // 
            this.шейкерныйМетодToolStripMenuItem.Name = "шейкерныйМетодToolStripMenuItem";
            this.шейкерныйМетодToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.шейкерныйМетодToolStripMenuItem.Text = "Шейкерный метод";
            this.шейкерныйМетодToolStripMenuItem.Click += new System.EventHandler(this.шейкерныйМетодToolStripMenuItem_Click);
            // 
            // методШеллаToolStripMenuItem
            // 
            this.методШеллаToolStripMenuItem.Name = "методШеллаToolStripMenuItem";
            this.методШеллаToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.методШеллаToolStripMenuItem.Text = "Метод Шелла";
            this.методШеллаToolStripMenuItem.Click += new System.EventHandler(this.методШеллаToolStripMenuItem_Click);
            // 
            // рандомныйСписокToolStripMenuItem
            // 
            this.рандомныйСписокToolStripMenuItem.Name = "рандомныйСписокToolStripMenuItem";
            this.рандомныйСписокToolStripMenuItem.Size = new System.Drawing.Size(172, 20);
            this.рандомныйСписокToolStripMenuItem.Text = "Создать рандомный список";
            this.рандомныйСписокToolStripMenuItem.Click += new System.EventHandler(this.рандомныйСписокToolStripMenuItem_Click);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 519);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Bauhaus 93", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form";
            this.Text = "Сортировки";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem сортировкаСпискаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem методПрямогоВыбораToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem методПузырькаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem методПрямогоВключенияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шейкерныйМетодToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem методШеллаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem рандомныйСписокToolStripMenuItem;
    }
}

