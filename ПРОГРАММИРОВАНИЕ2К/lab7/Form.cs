﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab7
{
    public partial class Form : System.Windows.Forms.Form
    {
        Computers computers;
        public Form()
        {
            InitializeComponent();
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToOrderColumns = true;
            dataGridView1.AllowUserToDeleteRows = true;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            computers = new Computers();
            dataGridView1.DataSource = computers.array;
        }

        private void методПрямогоВыбораToolStripMenuItem_Click(object sender, EventArgs e)
        {
            computers.selectionSort();
            dataGridView1.Refresh();
        }

        private void методПузырькаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            computers.bubbleSort();
            dataGridView1.Refresh();
        }

        private void методПрямогоВключенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            computers.inclusionSort();
            dataGridView1.Refresh();
        }

        private void шейкерныйМетодToolStripMenuItem_Click(object sender, EventArgs e)
        {
            computers.shakerSort();
            dataGridView1.Refresh();
        }

        private void методШеллаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            computers.shellSort();
            dataGridView1.Refresh();
        }

        private void рандомныйСписокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Type colorType = typeof(System.Drawing.Color);
            PropertyInfo[] propInfos = colorType.GetProperties(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public);

            computers = new Computers();
            
            for (int i = 0; i < 20; i++)
            {
                computers.Add(new Computer(new string(Enumerable.Repeat(chars, random.Next(3, 8))
                  .Select(s => s[random.Next(s.Length)]).ToArray()),
                  propInfos[random.Next(1,140)].Name,
                  random.Next(1, 500))
                  );
            }

            dataGridView1.DataSource = computers.array;
            dataGridView1.Refresh();
            computers.Length = computers.array.Count;
        }
    }
}
