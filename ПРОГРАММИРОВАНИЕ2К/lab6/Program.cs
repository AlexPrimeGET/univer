﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    class Computer
    {
        public string firm;
        public string color;
        public double outputPower;
        public Computer() { }
        public Computer(string firm, string color, double outputPower)
        {
            this.firm = firm;
            this.color = color;
            this.outputPower = outputPower;
        }
        public static bool operator <(Computer a, Computer b) { if (a.outputPower < b.outputPower) return true; else return false; }
        public static bool operator >(Computer a, Computer b) { if (a.outputPower > b.outputPower) return true; else return false; }
        public void setData(string firm, string color, double outputPower)
        {
            this.firm = firm;
            this.color = color;
            this.outputPower = outputPower;
        }
        public void copyDataTo(Computer computer)
        {
            computer.firm = this.firm;
            computer.color = this.color;
            computer.outputPower = this.outputPower;
        }
    }
    class Computers
    {

        public Computer[] array;
        public int Length;
        public Computers(int length)
        {
            this.Length = length;
            this.array = new Computer[length];
            for (int i = 0; i < length; i++)
            {
                this.array[i] = new Computer();
            }
        }
        public Computers(Computer[] array)
        {
            this.Length = array.Length;
            this.array = array;
        }
        public Computer this[int index]
        {
            get
            {
                return this.array[index];
            }
            set
            {
                this.array[index] = value;
            }
        }

        public void selectionSort()
        {
            Computer min = new Computer();
            this.array[0].copyDataTo(min);
            int minIndex;

            for (int i = 0; i < this.Length; i++)
            {
                minIndex = i;
                for (int j = i; j < this.Length; j++)
                {
                    if (this.array[j] < this.array[minIndex])
                    {
                        minIndex = j;
                    }
                }
                this.array[minIndex].copyDataTo(min);
                this.array[i].copyDataTo(this.array[minIndex]);
                min.copyDataTo(this.array[i]);
            }
        }
        public void bubbleSort()
        {
            Computer replacer = new Computer();
            for (int i = this.Length-2; i >= 0; i--)
            {
                for (int j = 0; j <= i; j++)
                {
                    if (this.array[j] > this.array[j+1])
                    {
                        this.array[j].copyDataTo(replacer);
                        this.array[j + 1].copyDataTo(this.array[j]);
                        replacer.copyDataTo(this.array[j + 1]);
                    }
                }
            }
        }
        public void inclusionSort()
        {
            Computer current = new Computer();
            int index;
            for (int i = 1; i < this.Length; i++)
            {
                this.array[i].copyDataTo(current);
                index = i;
                while ((index > 0) && (array[index - 1] > current))
                {
                    array[index - 1].copyDataTo(array[index]);
                    index--;
                }
                current.copyDataTo(array[index]);
            }
        }
        public void shakerSort()
        {
            Computer replacer = new Computer();
            for (int i = 0; i < (array.Length / 2) + (array.Length % 2); i++)
            {
                for (int j = i; j < (array.Length - 1); j++)
                {
                    if(array[j] > array[j + 1])
                    {
                        array[j].copyDataTo(replacer);
                        array[j + 1].copyDataTo(array[j]);
                        replacer.copyDataTo(array[j + 1]);
                    }
                }
                for (int j = array.Length - 1 - i; j > i; j--)
                {
                    if (array[j] < array[j - 1])
                    {
                        array[j].copyDataTo(replacer);
                        array[j - 1].copyDataTo(array[j]);
                        replacer.copyDataTo(array[j - 1]);
                    }
                }
            }
        }
        public void shellSort()
        {
            Computer replacer = new Computer();
            for (int s = array.Length / 2; s > 0; s /= 2)
                for (int i = s; i < array.Length; i++)
                    for (int j = i - s; j >= 0 && array[j] > array[j + s]; j -= s)
                    {
                        array[j].copyDataTo(replacer);
                        array[j + s].copyDataTo(array[j]);
                        replacer.copyDataTo(array[j + s]);
                    }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Computers comps = new Computers(new Computer[] { new Computer("a", "a", 20), new Computer("a", "a", 5), new Computer("a", "a", 8), new Computer("a", "a", 40), new Computer("a", "a", 50), new Computer("a", "a", 45) });
            Stopwatch startTime = Stopwatch.StartNew();
            comps.selectionSort();
            startTime.Stop();
            TimeSpan resultTime = startTime.Elapsed;
            Console.WriteLine("Сортировка методом прямого выбора: "+String.Format("{0}",resultTime.Ticks));

            comps = new Computers(new Computer[] { new Computer("a", "a", 20), new Computer("a", "a", 5), new Computer("a", "a", 8), new Computer("a", "a", 40), new Computer("a", "a", 50), new Computer("a", "a", 45) });
            startTime = Stopwatch.StartNew();
            comps.bubbleSort();
            startTime.Stop();
            resultTime = startTime.Elapsed;
            Console.WriteLine("Сортировка методом пузырька: "+String.Format("{0}", resultTime.Ticks));

            comps = new Computers(new Computer[] { new Computer("a", "a", 20), new Computer("a", "a", 5), new Computer("a", "a", 8), new Computer("a", "a", 40), new Computer("a", "a", 50), new Computer("a", "a", 45) });
            startTime = Stopwatch.StartNew();
            comps.inclusionSort();
            startTime.Stop();
            resultTime = startTime.Elapsed;
            Console.WriteLine("Сортировка методом прямого включения: "+String.Format("{0}", resultTime.Ticks));

            comps = new Computers(new Computer[] { new Computer("a", "a", 20), new Computer("a", "a", 5), new Computer("a", "a", 8), new Computer("a", "a", 40), new Computer("a", "a", 50), new Computer("a", "a", 45) });
            startTime = Stopwatch.StartNew();
            comps.shakerSort();
            startTime.Stop();
            resultTime = startTime.Elapsed;
            Console.WriteLine("Шейкерная сортирвока: "+String.Format("{0}", resultTime.Ticks));

            comps = new Computers(new Computer[] { new Computer("a", "a", 20), new Computer("a", "a", 5), new Computer("a", "a", 8), new Computer("a", "a", 40), new Computer("a", "a", 50), new Computer("a", "a", 45) });
            startTime = Stopwatch.StartNew();
            comps.shellSort();
            startTime.Stop();
            resultTime = startTime.Elapsed;
            Console.WriteLine("Сортировка методом Шелла: "+String.Format("{0}", resultTime.Ticks));
            Console.ReadLine();
        }
    }
}
