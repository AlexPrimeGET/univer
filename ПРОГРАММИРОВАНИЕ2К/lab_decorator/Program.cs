﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal animal = new Animal();
            animal.move();
            Console.WriteLine();
            HerbivoreAnimalDecorator animal1 = new HerbivoreAnimalDecorator(animal);
            animal1.move();
            Console.WriteLine();
            PredatorAnimalDecorator animal2 = new PredatorAnimalDecorator(animal1);
            animal2.move();
            Console.Read();
        }
    }
}
