﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decorator
{
    interface Entity
    {
        void move();
    }
    class Animal : Entity
    {
        public void move()
        {
            Console.WriteLine("IM MOVING");
        }
    }
    abstract class AnimalDecorator : Entity
    {
        Entity animal;
        public AnimalDecorator(Entity animal)
        {
            this.animal = animal;
        }
        public virtual void move()
        {
            animal.move();
        }
    }

    class HerbivoreAnimalDecorator : AnimalDecorator
    {
        public HerbivoreAnimalDecorator(Entity animal) : base(animal){}

        public override void move()
        {
            base.move();
            beVigilant();
        }
        private void beVigilant()
        {
            Console.WriteLine("I am wary of predators");
        }
    }
    class PredatorAnimalDecorator : AnimalDecorator
    {
        public PredatorAnimalDecorator(Entity animal) : base(animal) { }
        public override void move()
        {
            base.move();
            hunt();
        }
        public void hunt()
        {
            Console.WriteLine("Im searching for prey!!");
        }
    }
}
