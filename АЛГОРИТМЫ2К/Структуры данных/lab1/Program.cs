﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            List<int> lst = new List<int> { 1 , -5, -4, 12, 2, -9, 0, -6 };
            Console.WriteLine(String.Join(", ", lst));
            while (i < lst.Count)
            {
                if (lst[i] < 0)
                {
                    i++;
                    lst.Insert(i,0);
                }
                i++;
            }
            Console.WriteLine(String.Join(", ", lst));
            Console.ReadKey();
        }
    }
}
