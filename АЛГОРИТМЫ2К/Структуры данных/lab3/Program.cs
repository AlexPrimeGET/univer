﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> l = new List<double> { 1 , 2 , -5 , -3, 2, -1 };

            Console.WriteLine(String.Join(", ", l));

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i] < 0)
                {
                    i++;
                    l.Insert(i,0);
                }
            }

            Console.WriteLine(String.Join(", ", l));

            Console.ReadLine();
        }
    }
}
