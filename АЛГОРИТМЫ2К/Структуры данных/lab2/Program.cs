﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> s1 = new Stack<int>();
            Stack<int> s2 = new Stack<int>();

            Queue<int> l = new Queue<int>();

            for (int i = 0; i < 10; i++)
            {
                l.Enqueue(i * Convert.ToInt32(Math.Pow(-1,i)));
            }

            Console.WriteLine(String.Join(", ", l));

            var length = l.Count;
            int v;
            for (int i = 0; i < length; i++)
            {
                v = l.Dequeue();
                if (v < 0)
                {
                    s1.Push(v);
                }
                else
                {
                    s2.Push(v);
                }
            }

            Console.WriteLine(String.Join(", ", s1));
            Console.WriteLine(String.Join(", ", s2));

            Console.ReadLine();
        }
    }
}
